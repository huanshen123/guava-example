package com.guavause.ordering;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * @author kxd
 * @date 2018/11/26 17:16
 * description:
 */
public class OrderingUse {

    Ordering<String> byLengthOrdering = new Ordering<String>() {
        @Override
        public int compare(@Nullable String left, @Nullable String right) {
            return Ints.compare(left.length(), right.length());
        }
    };

    Ordering<Foo> ordering = Ordering.natural().nullsFirst().onResultOf(new Function<Foo, String>() {
        @Override
        public String apply(@Nullable Foo foo) {
            return foo.sortedBy;
        }
    });

    @Test
    public void test() {
        List list = Lists.newArrayList();
        assertTrue(byLengthOrdering.reverse().isOrdered(list));
    }


    @Test
    public void testObjectsEquals() {
        Objects.equal("a", "a");
        Objects.equal(null, "a");
        Objects.equal("a", null);
        Objects.equal(null, null);
    }

    @Test
    public void testToString() {
        System.out.println(MoreObjects.toStringHelper(Foo.class).add("x", 1).toString());
        MoreObjects.toStringHelper(Foo.class).add("x", 1).toString();
    }


    /*public int compareTo(Foo that) {
        return ComparisonChain.start().compare(this.aString, that.aString)
                .compare(this.anInt, that.anInt)
                .compare(this.anEnum, that.anEnum, Ordering.natural().<T>nullsLast())
                .result();

    }*/
}

