package com.guavause.cache;

import com.google.common.cache.*;
import java.util.concurrent.TimeUnit;

/**
 * @author kxd
 * @date 2018/12/21 11:05
 * description:
 */
public class CreateGuavaCache {

    /**
     * 创建缓存
     * @return
     */
    public Cache<String, String> getCache() {
        CacheLoader<String, String> loader = new CacheLoader<String, String>() {

            @Override
            public String load(String s) throws Exception {
                return null;
            }
        };
        RemovalListener<String, String> removalListener = new RemovalListener<String, String>() {

            @Override
            public void onRemoval(RemovalNotification<String, String> removalNotification) {
                System.out.println("remove info ...");
            }
        };
        //使用CacheBuilder方法进行构建实例， 添加初始化方法，过期时间，移除监听等属性
        return CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.MINUTES)
                .removalListener(removalListener)
                .build(loader);
    }
}
