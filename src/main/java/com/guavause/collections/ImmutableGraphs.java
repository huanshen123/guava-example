package com.guavause.collections;

import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import org.junit.Test;

/**
 * @author kxd
 * @date 2018/12/5 17:15
 * description:
 */
public class ImmutableGraphs {

    @Test
    public void test(){
        Graph graph= GraphBuilder.undirected().build();
        ImmutableGraph<Integer> immutableGraph=
                ImmutableGraph.copyOf(graph);
    }
}
