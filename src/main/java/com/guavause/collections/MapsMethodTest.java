package com.guavause.collections;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author kxd
 * @date 2018/12/3 16:41
 * description:
 */
public class MapsMethodTest {
    @Test
    public void test01() {
        //创建一个Iterable对象
        Iterable<String> strings = Iterables.concat();
        ImmutableMap<Integer, String> stringByIndex = Maps.uniqueIndex(strings, new Function<String, Integer>() {
            @Override
            public Integer apply(@Nullable String s) {
                return s.length();
            }
        });
    }

    @Test
    public void test02() {
        Map<String, Integer> left = ImmutableMap.of("a", 1, "b", 2, "c", 3);
        Map<String, Integer> right = ImmutableMap.of("b", 2, "c", 4, "d", 5);
        MapDifference<String, Integer> diff = Maps.difference(left, right);
        System.out.println(diff.entriesInCommon());
        System.out.println(diff.entriesDiffering());
        System.out.println(diff.entriesOnlyOnLeft());
        System.out.println(diff.entriesOnlyOnRight());

        /**
         * {b=2}
         {c=(3, 4)}
         {a=1}
         {d=5}
         */

    }

    @Test
    public void test04() {
        Multiset<String> multiset1 = HashMultiset.create();

        multiset1.add("a", 2);
        Multiset<String> multiset2 = HashMultiset.create();
        multiset2.add("a", 5);
        multiset1.containsAll(multiset2);
        Multisets.containsOccurrences(multiset1, multiset2);
    }

    @Test
    public void test05() {
        Multiset<String> multiset = HashMultiset.create();
        multiset.add("a", 3);
        multiset.add("b", 5);
        multiset.add("c", 1);
        ImmutableMultiset<String> highestCountFirst = Multisets.copyHighestCountFirst(multiset);
    }

    @Test

    public void test06() {
        ImmutableSet<String> digits = ImmutableSet.of("zero", "one", "two", "three", "four",
                "five", "six", "seven", "eight", "nine");
        Function<String, Integer> lengthFunction = new Function<String, Integer>() {
            @Override
            public Integer apply(@Nullable String s) {
                return s.length();
            }
        };
        //按照长度对map中的元素进行了分类
        ImmutableListMultimap<Integer, String> digitsByLength = Multimaps.index(digits, lengthFunction);
        System.out.println(digitsByLength);
    }

    @Test
    public void test07() {
        ArrayListMultimap<String, Integer> multimap = ArrayListMultimap.create();
        multimap.putAll("b", Ints.asList(2, 4, 6));
        multimap.putAll("a", Ints.asList(4, 2, 1));
        multimap.putAll("c", Ints.asList(2, 5, 3));

        TreeMultimap<Integer, String> inverse = Multimaps.invertFrom(multimap, TreeMultimap.<Integer, String>create());
        System.out.println(inverse);
        /**
         * {1=[a], 2=[a, b, c], 3=[c], 4=[a, b], 5=[c], 6=[b]}

         */

    }

    @Test
    public void test08() {
        Map<String, Integer> map = ImmutableMap.of("a", 1, "b", 1, "c", 2);
        SetMultimap<String, Integer> multimap = Multimaps.forMap(map);
        System.out.println(multimap);
        //{a=[1], b=[1], c=[2]}

        Multimap<Integer, String> inverse = Multimaps.invertFrom(multimap, HashMultimap.<Integer, String>create());
        System.out.println(inverse);
        //{1=[a, b], 2=[c]}
    }

    @Test
    public void test09() {
        ListMultimap<String, Integer> myMultimap = Multimaps.newListMultimap(
                Maps.<String, Collection<Integer>>newTreeMap(), new Supplier<List<Integer>>() {
                    @Override
                    public List<Integer> get() {
                        return Lists.newLinkedList();
                    }
                }
        );
    }

    @Test
    public void test10() {
        Table<String, Character, Integer> table = Tables.newCustomTable(
                Maps.<String, Map<Character, Integer>>newLinkedHashMap(),
                new Supplier<Map<Character, Integer>>() {
                    @Override
                    public Map<Character, Integer> get() {
                        return Maps.newLinkedHashMap();
                    }
                }
        );
    }


}
