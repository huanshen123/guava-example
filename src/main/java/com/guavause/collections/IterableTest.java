package com.guavause.collections;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author kxd
 * @date 2018/11/30 11:44
 * description:
 */
public class IterableTest {

    public void test01() {
        //concatenated has Elements 1,2,3,4,5,6
        Iterable<Integer> concatenated = Iterables.concat(Ints.asList(1, 2, 3), Ints.asList(4, 5, 6));
        HashSet<String> myLinkedHashSet = Sets.newLinkedHashSet();
        String lastAdded = Iterables.getLast(myLinkedHashSet);
        Set thisSetIsDefinitelyASingleton = Sets.newConcurrentHashSet();
        String theElement = (String) Iterables.getOnlyElement(thisSetIsDefinitelyASingleton);
    }
}
