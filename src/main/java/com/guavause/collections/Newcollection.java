package com.guavause.collections;

import com.google.common.collect.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kxd
 * @date 2018/11/27 13:53
 * description:
 */
public class Newcollection {


    @Test
    public void test() {
        String[] words = new String[5000];
        Map<String, Integer> counts = new HashMap<String, Integer>();
        for (String word : words) {
            Integer count = counts.get(word);
            if (count == null) {
                counts.put(word, 1);
            } else {
                counts.put(word, count + 1);
            }
        }
    }

    @Test
    public void test2() {
        BiMap<String, Integer> userId = HashBiMap.create();
        String userForId = userId.inverse().get(2);
    }

    @Test
    public void test3() {
        ClassToInstanceMap<Number> numberDefaults = MutableClassToInstanceMap.create();
        numberDefaults.putInstance(Integer.class, Integer.valueOf(0));
    }

    @Test
    public void test4() {
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        //{1,10}
        rangeSet.add(Range.closed(1, 10));
        rangeSet.add(Range.closedOpen(11, 15));
        rangeSet.add(Range.closedOpen(15, 20));
        rangeSet.add(Range.openClosed(0, 0));
        rangeSet.remove(Range.open(5, 10));
    }

}
