package com.guavause.collections;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author kxd
 * @date 2018/11/30 11:59
 * description:
 */
public class ListsTest {

    @Test
    public void test01() {
        List<Integer> countUp = Ints.asList(1, 2, 3);
        List<Integer> countDown = Lists.reverse(countUp);
        Assert.assertTrue(countDown.get(0).equals(3));
    }
}
