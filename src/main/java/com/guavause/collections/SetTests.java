package com.guavause.collections;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;

/**
 * @author kxd
 * @date 2018/11/30 18:56
 * description:
 */
public class SetTests {

    public void test01() {
        Set<String> wordsWithPrimeLength = ImmutableSet.of("one", "two");
        Set<String> primes = ImmutableSet.of("two", "three");
        //两个集合的交集
        Sets.SetView<String> intersection = Sets.intersection(primes, wordsWithPrimeLength);
        intersection.immutableCopy();
    }

    public void test02() {
        Set<String> animals = ImmutableSet.of("gerbil", "hamster");
        Set<String> fruits = ImmutableSet.of("apple", "orange", "banana");

        Set<List<String>> product = Sets.cartesianProduct(animals, fruits);
        // {{"gerbil", "apple"}, {"gerbil", "orange"}, {"gerbil", "banana"},
        //  {"hamster", "apple"}, {"hamster", "orange"}, {"hamster", "banana"}}
        Set<Set<String>> animalSets = Sets.powerSet(animals);
        // {{}, {"gerbil"}, {"hamster"}, {"gerbil", "hamster"}}
    }
}
