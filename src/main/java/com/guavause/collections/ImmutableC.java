package com.guavause.collections;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.Collection;
import java.util.Set;

/**
 * @author kxd
 * @date 2018/11/27 10:12
 * description:
 */
public class ImmutableC {

    String WEBSAFE_COLORS = "a";
    /**
     * of方式创建不可变对象
     */
    public static final ImmutableSet<String> COLOR_NAMES = ImmutableSet.of(
            "red",
            "orange",
            "yellow",
            "green",
            "blue",
            "purple"
    );

    ImmutableSet<String> foobar = ImmutableSet.of("foo", "bar", "baz");
    public ImmutableList doCopy(Collection<String> collection) {
        return ImmutableList.copyOf(collection);

    }

    @Test
    public void testImmutableList(){
        doCopy(foobar);
    }

    /**
     * builder方式创建不可变对象
     */
    public static final ImmutableSet<Color> GOOGLE_COLORS = ImmutableSet.<Color>builder()
            .add(new Color(0, 191, 255))
            .build();

    @Test

    public void test() {

    }

    class Foo {
        final ImmutableSet<Bar> bars;

        Foo(Set<Bar> bars) {
            this.bars = ImmutableSet.copyOf(bars);

        }
    }

    class Bar {

    }

    static class Color {
        static int a;
        static int b;
        static int c;
        static Color WEBSAFE_COLORS = getInstance(a, b, c);

        public Color(int a, int b, int c) {
        }

        public static Color getInstance(int a, int b, int c) {
            return new Color(a, b, c);
        }
    }
}
