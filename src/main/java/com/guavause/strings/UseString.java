package com.guavause.strings;

import com.google.common.base.*;
import org.junit.Test;
import java.util.Arrays;

/**
 * @author kxd
 * @date 2019/1/11 10:46
 * description:
 */
public class UseString {

    @Test
    public void testJoiner() {
        Joiner joiner0 = Joiner.on(";").skipNulls();
        Joiner joiner = Joiner.on(";").useForNull("null");
        String res = joiner.join("Harry", null, "Ron", "Hermione");
        System.out.println(res);
        String info = Joiner.on(",").join(Arrays.asList(1, 5, 7));
        System.out.println(info);
    }

    @Test
    public void testSplitter() {
        Splitter.on(",").trimResults().omitEmptyStrings().split("foo,bar,,  qux");
        Splitter.on(",").omitEmptyStrings().split("a,c,,d");
        Splitter.on(",").trimResults(CharMatcher.whitespace()).split("a,b ,c , d");
        Iterable<String> iterable = Splitter.on(",").trimResults(CharMatcher.is('_')).split("_a__,_b ,_c , d");
        System.out.println(iterable.toString());
        Iterable<String> result = Splitter.on(',').limit(3).split("a,b , c,d,e,f");
        System.out.println(result);
        // Splitter.MapSplitter;
    }

    @Test
    public void testCharSet() {
        String test = "testString";
        //不推荐这样
        try {
            test.getBytes("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //应该这样
        test.getBytes(Charsets.UTF_8);
    }

    @Test
    public void testCaseFormat() {
        String result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "LAUCH_TIME");
        System.out.println(result);
    }

}
