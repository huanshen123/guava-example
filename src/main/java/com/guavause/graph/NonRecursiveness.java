package com.guavause.graph;

import com.google.common.base.Objects;

import java.util.Set;

/**
 * @author kxd
 * @date 2018/12/5 19:56
 * description:
 */
public class NonRecursiveness {
    public final class Node<T> {
        T value;
        Set<Node<T>> successors;

        @Override
        public boolean equals(Object o) {

            Node<T> other = (Node<T>) o;
            return Objects.equal(value, other.value)
                    && Objects.equal(successors, other.successors);

        }

        @Override
        public int hashCode() {
            return Objects.hashCode(value, successors);
        }
    }
}
