package com.guavause.pool;

/**
 * @author: User
 * @date: 2023/1/12
 * @Description:此类用于xxx
 */

public class PoolSchedUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.err.println(e.getLocalizedMessage());
    }
}
