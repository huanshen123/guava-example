package com.guavause.pool;

import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

/**
 * @author: User
 * @date: 2023/1/12
 * @Description:此类用于xxx
 */
public class ThreadPoolGuava {
    private static final int PROCESSORS= Runtime.getRuntime().availableProcessors();

    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(15);
        ThreadFactoryBuilder threadFactoryBuilder=new ThreadFactoryBuilder();
        threadFactoryBuilder.setNameFormat("async.pos.cache");
        PoolSchedUncaughtExceptionHandler poolSchedUncaughtExceptionHandler = new PoolSchedUncaughtExceptionHandler();
        threadFactoryBuilder.setUncaughtExceptionHandler(poolSchedUncaughtExceptionHandler);
        threadFactoryBuilder.build();
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(PROCESSORS+1,threadFactoryBuilder.build());
        ScheduledExecutorService scheduledExecutorService = MoreExecutors.getExitingScheduledExecutorService(scheduledThreadPoolExecutor);
        scheduledExecutorService.scheduleWithFixedDelay(() -> {
            countDownLatch.countDown();
            System.out.println("ok");
        }, 50, 480, TimeUnit.MILLISECONDS);
        countDownLatch.await();
    }
}
