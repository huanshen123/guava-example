并发编程之Service框架
===
- ScheduledExecutorService 定时执行线程池
- ScheduledThreadPoolExecutor 是java并发中自带的定时线程池类
- Thread.UncaughtExceptionHandler 线程池中多个线程异常后，异常处理器实现这个接口方法
- ThreadFactoryBuilder 用来创建线程工厂 
- 使用com.google.common.util.concurrent.MoreExecutors来获取ExecutorService


------
[返回目录](/README.md)